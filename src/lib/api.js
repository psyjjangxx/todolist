import axios from 'axios';
//import queryString from 'query-string';

export const getTodoList = () => axios.get(`/todo`);
export const insertTodo = ({id, text, done}) => axios.post('/todo',{id, text,done});
export const getId = () => axios.get(`/todo`);
export const removeTodo = (id) => axios.delete(`/todo/${id}`);
export const updateTodo = ({id, done}) => axios.patch(`todo/${id}`, {done});
