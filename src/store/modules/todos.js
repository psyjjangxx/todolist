import { Map, List, fromJS } from 'immutable';
import { handleActions, createAction } from 'redux-actions';
import { pender } from 'redux-pender';
import * as api from 'lib/api';

//const INSERT = 'todos/INSERT';
//const TOGGLE = 'todos/TOGGLE';
//const REMOVE = 'todos/REMOVE';
const UPDATE_TODO = 'todos/UPDATE_TODO';
const GET_TODO_LIST = 'todos/GET_TODO_LIST';
const INSERT_TODO = 'todos/INSERT_TODO';
const GET_ID = 'todos/GET_ID';
const REMOVE_TODO = 'todos/REMOVE_TODO';

//export const insert = createAction(INSERT);
//export const toggle = createAction(TOGGLE);
//export const remove = createAction(REMOVE);
export const updateTodo = createAction(UPDATE_TODO, api.updateTodo, meta => meta);
export const getTodoList = createAction(GET_TODO_LIST, api.getTodoList, meta => meta);
export const insertTodo = createAction(INSERT_TODO, api.insertTodo);
export const getId = createAction(GET_ID, api.getId, meta => meta);
export const removeTodo = createAction(REMOVE_TODO, api.removeTodo, meta => meta);

const initialState = Map({
  todo:List(),
});

export default handleActions({
  // [INSERT]: (state, action) => {
  //   /* payload 안에 있는 id, text, done에 대한 레퍼런스를 만들어줍니다.
  //   레퍼런스를 만들지 않고, 바로 push(Map(action.payload))를 해도 되지만,
  //   나중에 이 코드를 보게 됐을 때, 
  //   이 액션이 어떤 데이터를 처리하는지 쉽게 보기 위해서 하는 작업입니다. */
  //   const { id, text, done } = action.payload;

  //   return state.push(Map({
  //     id,
  //     text,
  //     done
  //   }));
  // },
  ...pender({
    type: UPDATE_TODO,
    onSuccess: (state, action) => {
      //const { data : todo } = action.payload;
        //alert(todo.length);
        //return state.set('todo', fromJS(todo));
        return ;
    }
  }),
  ...pender({
    type: INSERT_TODO,
    onSuccess: (state, action) => {
      const { _id } = action.payload.data;
      return state.set('postId', _id);
    }
  }),
  // [TOGGLE]: (state, action) => {
  //   const { payload: id } = action;
  //   // = const id = action.payload;
  //   /* 비구조화 할당을 통하여 id라는 레퍼런스에 action.payload란 값을 넣습니다.
  //   이 작업이 필수는 아니지만, 나중에 이 코드를 보게 되었을 때 여기서의 payload가
  //   어떤 값을 의미하는지 이해하기 쉬워집니다. */

  //   // 전달받은 id 를 가지고 index 를 조회합니다.
  //   const index = state.findIndex(todo => todo.get('id') === id);

  //   // updateIn을 통해 현재 값을 참조하여 반대값으로 설정합니다.
  //   return state.updateIn([index, 'done'], done => !done);
  //   /* updateIn을 사용하지 않는다면 다음과 같이 작성할 수도 있습니다.
  //   return state.setIn([index, 'done'], !state.getIn([0, index]));
  //   어떤 코드가 더 편해 보이나요? 둘 중에 여러분이 맘에 드는 코드로 작성하면 됩니다.
  //   */
  // },
  // [REMOVE]: (state, action) => {
  //   const { payload: id } = action;
  //   const index = state.findIndex(todo => todo.get('id') === id);
  //   return state.delete(index);
  // },
  ...pender({
    type: REMOVE_TODO,
    onSuccess: (state, action) => {
      const { data : todo } = action.payload;
        //alert(todo.length);
        return state.set('todo', fromJS(todo));
    }
  }),

  ...pender({
    type: GET_TODO_LIST,
    onSuccess: (state, action) => {
        const { data : todo } = action.payload;
        //alert(todo.length);
        return state.set('todo', fromJS(todo));
    }
  }),
  ...pender({
    type: GET_ID,
    onSuccess: (state, action) => {
        const { data : todo } = action.payload;
        return state.set('getId', todo.length);
    }
  })
}, initialState);